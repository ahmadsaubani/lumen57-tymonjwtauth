# Lumen PHP Framework With Tymon Jwt Authentication and Swagger Api Docs

```` installation````
* Run ``Composer Install`` always do composer install after clone this repo
* ``copy .env.example to .env`` if want to try auth
* setup a database connection on ``.env`` if want to try auth

# Generate Swagger Api Docs
* Run ``php artisan swagger-lume:publish-config`` to publish configs (config/swagger-lume.php)
* Run ``php artisan swagger-lume:publish-views`` to publish views (resources/views/vendor/swagger-lume)
* Run ``php artisan swagger-lume:publish`` to publish everything
* Run ``php artisan swagger-lume:generate`` to generate docs

# Generate Jwt secret
* Run ``php artisan jwt:generate`` if dont have a jwt key on .env

# Migrate

* Run ``php artisan migrate:fresh`` to drop tables and up the tables

# Seeds Database
* Run ``php artisan db:seed to seeders`` the table with data dummy faker

# Phpunit test
* Run ``php vendor/bin/phpunit`` to run a phpunit test
